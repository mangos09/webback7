<?php
session_start();
include('csrf.class.php');
$csrf = new csrf();
// Генерируем Token id
$token_id = $csrf->get-token_id();
$token_value = $csrf->get_token($token_id);

// Генерируем случайные имена для формы
$form_names = $csrf->get_token($token_id);

if (isset($_POST['login'])) {
    $login = $_POST['login'];
    if ($login == '') {
        unset($login);
    }
}
if (isset($_POST['pass'])) {
    $pass = $_POST['pass'];
    if ($pass == '') {
        unset($pass);
    }
}

//если пользователь не ввел логин или пароль, то выдаем ошибку и останавливаем скрипт
if (empty($login) || empty($pass)) {
    exit("Вы ввели не всю информацию, вернитесь назад и заполните все поля!");
}

//если логин и пароль введены, то обрабатываем их
$login = htmlspecialchars(trim($login));
$pass = htmlspecialchars(md5(trim($pass)));

// Подключаемся к базе данных
include("bd.php");

// Извлекаем из базы все данные о пользователе с введенным логином
$query = $db->prepare("SELECT * FROM `application` WHERE `login` = ?");
$query->execute([$login]);
$row = $query->fetch();
if (empty($row['login'])) {
    // Если пользователя с введенным логином не существует
    exit("Извините, введённый вами login или пароль неверный.");
}
else {
    // Если существует, то проверяем пароли
    if ($row['pass'] == $pass) {
        // Если пароли совпадают, то запускаем пользователю сессию. Он вошёл
        $_SESSION['login'] = $row['login']; 
        $_SESSION['id'] = $row['id'];
        echo "Вы успешно вошли на сайт! <a href='index.php'>Главная страница</a>";
    }
    else {
        exit("Извините, введённый вами login или пароль неверный.");
    }
}

?>